package utils;

import java.io.*;
import java.util.stream.Stream;

public class OutputStream {
    private String fileName;

    public OutputStream(String fileName) {
        this.fileName = fileName;
    }

    private Stream<String> readByteFromFile() throws IOException {
        File file = new File(this.fileName);
        FileReader fr = new FileReader(file);

        BufferedReader br = new BufferedReader(fr);
        return br.lines();
    }
}
