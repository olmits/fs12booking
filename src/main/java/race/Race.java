package race;

import java.util.Date;

public class Race {
    private String id;
    private Date schedule;
    private String destination;
    private Integer vacantPlaces;

    public Race(String id, Date schedule, String destination) {
        this.id = id;
        this.schedule = schedule;
        this.destination = destination;
        this.vacantPlaces = 0;
    }

    public Date getSchedule() {
        return this.schedule;
    }

    public String getDestination() {
        return this.destination;
    }

    public Integer getVacantPlaces() {
        return this.vacantPlaces;
    }

    public void printRaceInfo() {
        System.out.printf("Race %s to %s %s. Available seats: %d", this.id, this.destination, this.schedule.toString(), this.vacantPlaces);
    }
}
